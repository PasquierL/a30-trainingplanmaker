const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})

module.exports = {
  pwa: {
    start_url: ".",
    appleMobileWebAppCapable: 'yes',
    manifestOptions: {
      id: "/",
      //start_url: "/?standalone=true",
    },
    workboxPluginMode: 'GenerateSW',
  }
}